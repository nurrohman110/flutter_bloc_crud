import 'package:flutter_bloc_crud/customer/model/customer_model.dart';
import 'package:flutter_bloc_crud/customer/repository/customer_repository.dart';

class InsertUpdateBloc {
  CustomerRepository customerRepository;

  InsertUpdateBloc() {
    customerRepository = CustomerRepository();
  }

  Future<int> addProduct(CustomerModel customerModel) async {
    int result = await customerRepository.insertCustomer(customerModel);
    return result;
  }

  Future<int> updateProduct(CustomerModel customerModel) async{
    int result = await customerRepository.updateCustomer(customerModel);
    return result;
  }
}
