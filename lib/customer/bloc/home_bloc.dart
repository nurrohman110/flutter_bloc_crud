import 'dart:async';

import 'package:flutter_bloc_crud/customer/model/customer_model.dart';
import 'package:flutter_bloc_crud/customer/repository/customer_repository.dart';

class HomeBloc{
  CustomerRepository customerRepository;

  final listController = StreamController<List<CustomerModel>>();
  StreamSink<List<CustomerModel>> get _inList => listController.sink;
  Stream<List<CustomerModel>> get list => listController.stream;

  HomeBloc(){
    customerRepository = CustomerRepository();
  }

  getAllCustomer() async{
    List<CustomerModel> result = await customerRepository.getAllCustomer();
    _inList.add(result);
  }

  Future<int> deleteCustomer(int id) async{
    int result = await customerRepository.deleteCustomer(id);
    return result;
  }
}
