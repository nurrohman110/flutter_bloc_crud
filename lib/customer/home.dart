import 'package:flutter/material.dart';
import 'package:flutter_bloc_crud/customer/bloc/home_bloc.dart';
import 'package:flutter_bloc_crud/customer/insertupdate/insertUpdate.dart';
import 'package:flutter_bloc_crud/customer/model/customer_model.dart';

class CustomerHome extends StatefulWidget {
  @override
  _CustomerHomeState createState() => _CustomerHomeState();
}

class _CustomerHomeState extends State<CustomerHome> {
  final HomeBloc _bloc = HomeBloc();
  @override
  void initState() {
    super.initState();
    _bloc.getAllCustomer();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Customers'),
      ),
      body: StreamBuilder(
        stream: _bloc.list,
        initialData: List<CustomerModel>(),
        builder: (context, snapshot) {
          List<CustomerModel> listCustomer = snapshot.data;
          if (listCustomer.length == 0) {
            return Center(
              child: Text('Tidak ada data'),
            );
          }
          return ListView.separated(
            itemBuilder: (context, i) {
              return ListItem(_bloc, listCustomer[i]);
            },
            separatorBuilder: (context, i) => Divider(
              color: Colors.grey,
            ),
            itemCount: listCustomer.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => InsertUpdate(
                customerModel: CustomerModel().setId(0),
              ),
            ),
          );
        },
      ),
    );
  }
}

class ListItem extends StatelessWidget {
  final CustomerModel customerModel;
  final HomeBloc bloc;

  ListItem(this.bloc, this.customerModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(customerModel.customerName),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.center,
              child: Text(customerModel.customerPhone.toString()),
            ),
          ),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: GestureDetector(
                  onTap: () => gotoInsertUpdate(context, customerModel),
                  child: Icon(Icons.edit, color: Colors.blue),
                ),
              ),
              GestureDetector(
                onTap: () => dialogDelete(context, customerModel),
                child: Icon(Icons.delete, color: Colors.red),
              ),
            ],
          ))
        ],
      ),
    );
  }

  void gotoInsertUpdate(BuildContext context, CustomerModel customerModel) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => InsertUpdate(
          customerModel: customerModel,
        ),
      ),
    );
  }

  void dialogDelete(BuildContext context, CustomerModel customerModel) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text('Delete'),
        content: Text(
            'Are you sure want to delete "${customerModel.customerName}"?'),
        actions: <Widget>[
          FlatButton(
            onPressed: () async {
              int result = await bloc.deleteCustomer(customerModel.customerId);
              if (result == 200) {
                bloc.getAllCustomer();
              }
              Navigator.pop(context);
            },
            child: Text("Yes"),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("No"),
          ),
        ],
      ),
    );
  }
}
