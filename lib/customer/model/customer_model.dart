import 'dart:convert';

CustomerModel fromJson(String str) {
  final jsonData = jsonDecode(str);
  return CustomerModel.fromMap(jsonData);
}

String toJson(CustomerModel customerModel){
  final dyn = customerModel.toMap();
  return jsonEncode(dyn);
}

class CustomerModel {
  int customerId;
  String customerName;
  String customerCity;
  String customerPhone;

  CustomerModel({
    this.customerCity,
    this.customerId,
    this.customerName,
    this.customerPhone,
  });
  factory CustomerModel.fromMap(Map<String, dynamic> json) => CustomerModel(
        customerCity: json['customerCity'],
        customerId: json['customerId'],
        customerName: json['customerName'],
        customerPhone: json['customerPhone'],
      );

  Map<String, dynamic> toMap() => {
        'customerId': customerId,
        'customerName': customerName,
        'customerCity': customerCity,
        'customerPhone': customerPhone,
      };

  CustomerModel setId(int id){
    this.customerId = id;
    return this;
  }
  int getId(){
    return customerId;
  }
  String getName(){
    return customerName;
  }
}
