import 'dart:convert';

import 'package:flutter_bloc_crud/customer/model/customer_model.dart';
import 'package:http/http.dart' as http;

class CustomerRepository {
  // String urlApi = 'http://192.168.43.67/webservice_rekayasa_web/api/';
  String urlApi = '';
  Future<List<CustomerModel>> getAllCustomer() async {
    final url = '${urlApi}customerGet';
    final response = await http.get(url);
    Map<String, dynamic> dataMapping = jsonDecode(response.body);
    List<dynamic> result = dataMapping['customer'];
    final listCustomer = result.map((v) => CustomerModel.fromMap(v)).toList();
    return listCustomer;
  }

  Future<int> insertCustomer(CustomerModel customerModel) async {
    var data = {
      'customerName': customerModel.customerName,
      'customerPhone': customerModel.customerPhone,
      'customerCity': customerModel.customerCity,
    };
    print('data $data');
    final url = '${urlApi}customerPost';
    print('url $url');
    final response = await http.post(
      url,
      body: data,
    );
    return response.statusCode;
  }

  Future<int> deleteCustomer(int id) async {
    final url = '${urlApi}customerDelete?id=$id';
    final response = await http.delete(url);
    print('response body ${response.body}');
    return response.statusCode;
  }

  Future<int> updateCustomer(CustomerModel customerModel) async {
    var data = {
      'id': customerModel.customerId.toString(),
      'customerName': customerModel.customerName,
      'customerPhone': customerModel.customerPhone,
      'customerCity': customerModel.customerCity,
    };
    final url = '${urlApi}customerUpdate';
    final response = await http.post(url, body: data);
    return response.statusCode;
  }
}
