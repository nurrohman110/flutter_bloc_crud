import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc_crud/customer/bloc/home_bloc.dart';
import 'package:flutter_bloc_crud/customer/bloc/insertUpdate_bloc.dart';
import 'package:flutter_bloc_crud/customer/home.dart';
import 'package:flutter_bloc_crud/customer/model/customer_model.dart';
import 'package:http/http.dart' as http;

class InsertUpdate extends StatefulWidget {
  final CustomerModel customerModel;

  InsertUpdate({this.customerModel});
  @override
  _InsertUpdateState createState() => _InsertUpdateState();
}

class _InsertUpdateState extends State<InsertUpdate> {
  final InsertUpdateBloc insertUpdateBloc = InsertUpdateBloc();
  String title;
  final customerName = TextEditingController();
  final customerPhone = TextEditingController();
  final customerCity = TextEditingController();
  @override
  Widget build(BuildContext context) {
    if (widget.customerModel.getId() == 0) {
      title = 'Insert';
    } else {
      title = 'Update';
      customerName.text = widget.customerModel.customerName;
      customerPhone.text = widget.customerModel.customerPhone.toString();
      customerCity.text = widget.customerModel.customerCity;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
      ),
      body: Builder(
        builder: (context) => Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    title,
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Divider(
                color: Colors.grey,
              ),
              Expanded(
                child: Container(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child: TextFormField(
                            controller: customerName,
                            decoration:
                                InputDecoration(labelText: "Nama Customer"),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child: TextFormField(
                            controller: customerPhone,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Nomor Telepon Customer",
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: customerCity,
                            decoration: InputDecoration(
                              labelText: "Alamat Customer",
                            ),
                          ),
                        ),
                        FlatButton(
                          // onPressed: () => onSave(context),
                          onPressed: () {
                            onSave();
                          },
                          child: Text("Save"),
                        )
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  void onSave() async {
    String nama = customerName.text;
    String phone = customerPhone.text;
    String alamat = customerCity.text;
    int result = -1;
    if (nama != '' && phone != '' && alamat != '') {
      CustomerModel customerModel = CustomerModel(
        customerName: nama,
        customerCity: alamat,
        customerPhone: phone,
      );
      if (widget.customerModel.getId() == 0) {
        // insert
        widget.customerModel.setId(0);
        result = await insertUpdateBloc.addProduct(customerModel);
      } else {
        customerModel.setId(widget.customerModel.getId());
        result = await insertUpdateBloc.updateProduct(customerModel);
      }
    } else {
      showDialogNotifikasi('Tidak boleh kosong');
    }

    if (result > 200 || result < 300) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => CustomerHome(),
          ),
          (route) => false);
    } else {
      print('gagal');
    }
  }

  showDialogNotifikasi(String title) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Center(
            child: Text(title),
          ),
        );
      },
    );
  }
}
